import React from 'react';
import Note from './Note';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const NoteWarpper = styled.div`
    max-width: 800px;
    margin: 0 auto;
    margin-bottom: 2em;
    padding-bottom: 2em;
    border-bottom:1px solid #f0f0f0;
`;

const NoteFeed = ({ notes }) => {
    return(
        <div>
            {notes.map(note => (
                <NoteWarpper key={note.id}>
                    <Note note={note} />
                    <Link to={`note/${note.id}`}>Permalink</Link>
                </NoteWarpper>
            ))}
        </div>
    )
};

export default NoteFeed;