import React, { useEffect } from 'react';
import { useQuery, gql } from '@apollo/client';

import noteFeed from '../components/NoteFeed';
import { GET_MY_FAVORITE } from '../gql/query';
import NoteFeed from '../components/NoteFeed';

const Favorites = () => {
    useEffect(() => {
        //更新文档标题
        document.title = 'Favorites -- Notedly';
    });

    const { loading, error, data } = useQuery(GET_MY_FAVORITE);

    if (loading) {
        return 'Loading...';
    };
    if (error) {
        return `Error! ${error.message}`;
    };
    if (data.me.favorites.length !== 0) {
        return <NoteFeed notes={data.me.favorites}/>;
    }else{
        return <p>No favorites yet</p>;
    }
};

export default Favorites;